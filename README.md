## Users - API

Featuring:

- Docker v18.09.2
- Docker Compose v1.23.2
- Docker Machine v0.16.1
- Python 3.7.3

### HOW TO SETUP API OS X Instructions

1. Start new machine - `docker-machine create -d virtualbox dev;`
1. Configure your shell to use the new machine environment - `eval $(docker-machine env dev)`
1. Build images - `docker-compose build`
1. Start services - `docker-compose up -d`
2. Get Machine's IP 
> `docker-machine ip dev` 

then paste it into web/users/.env as DB_SERVICE

5. Start web manually 
> `cd web && python3 -m venv env && source env/bin/activate && python3.7 -m pip install --upgrade pip && pip3 install -r requirements.txt && python3 manage.py runserver` 

please upgrade pip3 if you encountered an error : 
> `python3.7 -m pip install --upgrade pip` then install the dependencies `pip3 install -r requirements.txt`

6. Create migrations - 
> `python3 manage.py makemigrations users && python3 manage.py migrate`

7. Refer to local http://127.0.0.1:8000 

### API Description

Endpoint -> http://127.0.0.1:8000/swagger-ui/

### Endpoint Description

GET users - GET http://127.0.0.1:8000/sirka/displayallusers
POST users - GET http://127.0.0.1:8000/sirka/displayuser 