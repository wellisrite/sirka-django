import coreapi, json
from rest_framework.filters import BaseFilterBackend
from sirka.models import Users
from sirka.serializers import UsersSerializer
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.validators import ValidationError
from django.db import IntegrityError


@api_view(['GET'])
def userList(request, format=None):
    """
    Sirka API - Get All users
    """
    users = Users.objects.all()
    serializer = UsersSerializer(users, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def userDetail(request):
    """
    Sirka API - Get One User
    id -- User id
    """
    data = request.POST

    if "id" not in data:
        return Response("Please input user id", status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    
    try:
        user = Users.objects.get(pk=data["id"])
    except Users.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = UsersSerializer(user)
    return Response(serializer.data)
