from rest_framework import serializers
from sirka.models import Users, LANGUAGE_CHOICES, STYLE_CHOICES

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['id', 'name']
        extra_kwargs = {
            'url': {'validators': []},
        }

